import androidx.compose.desktop.ui.tooling.preview.Preview
import androidx.compose.runtime.Composable
import androidx.compose.ui.unit.DpSize
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Window
import androidx.compose.ui.window.application
import androidx.compose.ui.window.rememberWindowState

fun main() = application {
    val windowState = rememberWindowState(size = DpSize(1100.dp, 750.dp))
    Window(onCloseRequest = ::exitApplication, state = windowState, title = "Journalist") {
        App()
    }
}

@Preview
@Composable
fun AppDesktopPreview() {
    App()
}