package st.journal

import App
import NextcloudLogin
import NextcloudLoginState
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.runtime.Composable
import androidx.compose.ui.platform.UriHandler
import androidx.compose.ui.tooling.preview.Preview
import com.nextcloud.android.sso.AccountImporter
import com.nextcloud.android.sso.exceptions.*
import com.nextcloud.android.sso.helper.SingleAccountHelper
import com.nextcloud.android.sso.ui.UiExceptionManager
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update

lateinit var appContext: Context

class MainActivity : ComponentActivity() {

    private val ssoAccount
        get() = try {
            SingleAccountHelper.getCurrentSingleSignOnAccount(this)
        } catch (e: NextcloudFilesAppAccountNotFoundException) {
            // UiExceptionManager.showDialogForException(this, e)
            null
        } catch (e: NoCurrentAccountSelectedException) {
            // UiExceptionManager.showDialogForException(this, e)
            null
        }

    private val nextcloudLoginState = MutableStateFlow<NextcloudLoginState>(NextcloudLoginState.ReadyForLogin)

    private val nextcloudLogin: NextcloudLogin = object : NextcloudLogin {
        override val state = nextcloudLoginState.asStateFlow()

        override suspend fun login(serverUrl: String?, uriHandler: UriHandler?) {
            openAccountChooser()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        appContext = applicationContext
        ssoAccount?.let { account ->
            nextcloudLoginState.update { NextcloudLoginState.LoggedIn(serverUrl = account.url) }
        }

        setContent {
            App(nextcloudLogin)
        }
    }

    private fun openAccountChooser() {
        try {
            AccountImporter.pickNewAccount(this)
        } catch (e: NextcloudFilesAppNotInstalledException) {
            UiExceptionManager.showDialogForException(this, e)
        } catch (e: AndroidGetAccountsPermissionNotGranted) {
            UiExceptionManager.showDialogForException(this, e)
        }
    }

    @Suppress("Deprecation")
    @Override
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        try {
            AccountImporter.onActivityResult(
                requestCode,
                resultCode,
                data,
                this
            ) { account ->
                // As this library supports multiple accounts we created some helper methods if you only want to use one.
                // The following line stores the selected account as the "default" account which can be queried by using
                // the SingleAccountHelper.getCurrentSingleSignOnAccount(context) method
                SingleAccountHelper.commitCurrentAccount(this, account.name)

                nextcloudLoginState.update { NextcloudLoginState.LoggedIn(serverUrl = account.url) }
            }
        } catch (e: AccountImportCancelledException) {
            println(e.message)
        }
    }

    @Override
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        AccountImporter.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

    /* private fun getActivitiesNew() {
        GlobalScope.launch {
            println(serverUrl)
            val res = test.get(serverUrl + "/ocs/v2.php/apps/activity/api/v2/activity")
            val body = res.bodyAsText()
            nextcloudState.update { it.copy(activities = body) }
        }
    }

    private fun getActivities() {
        val req = NextcloudRequest.Builder()
            .setMethod("GET")
            // .setParameter(parameters)
            .setUrl(Uri.encode("/ocs/v2.php/apps/activity/api/v2/activity", "/"))
            .build()
        val res = nextcloudApi.performNetworkRequestV2(req)
        val body = res.body.bufferedReader().use(BufferedReader::readText)
        runOnUiThread {
            nextcloudState.update { it.copy(activities = body) }
        }
    } */
}

@Preview
@Composable
fun AppAndroidPreview() {
    App()
}