package st.journal.api

/*
 * Copyright 2014-2019 JetBrains s.r.o and contributors. Use of this source code is governed by the Apache 2.0 license.
 */

import android.content.Context
import com.nextcloud.android.sso.model.SingleSignOnAccount
import io.ktor.client.*
import io.ktor.client.engine.*

/**
 * A configuration for the [Nextcloud] client engine.
 */
class NextcloudConfig : HttpClientEngineConfig() {

    var context: Context? = null

    var ssoAccount: SingleSignOnAccount? = null

    var delegate: HttpClient? = null
}
