package st.journal.api

/*
* Copyright 2014-2021 JetBrains s.r.o and contributors. Use of this source code is governed by the Apache 2.0 license.
*/

import android.content.Context
import android.util.Log
import com.google.gson.GsonBuilder
import com.nextcloud.android.sso.aidl.NextcloudRequest
import com.nextcloud.android.sso.api.AidlNetworkRequest
import com.nextcloud.android.sso.api.NextcloudAPI
import com.nextcloud.android.sso.api.Response
import com.nextcloud.android.sso.exceptions.TokenMismatchException
import com.nextcloud.android.sso.model.SingleSignOnAccount
import io.ktor.client.call.*
import io.ktor.client.engine.*
import io.ktor.client.plugins.*
import io.ktor.client.request.*
import io.ktor.http.*
import io.ktor.http.content.*
import io.ktor.util.*
import io.ktor.util.date.*
import io.ktor.utils.io.*
import it.niedermann.nextcloud.sso.urlhelper.fallbackPreviewSize
import it.niedermann.nextcloud.sso.urlhelper.getAbsoluteUrl
import it.niedermann.nextcloud.sso.urlhelper.getQueryParams
import kotlinx.coroutines.*
import okhttp3.internal.http.HttpMethod
import okio.BufferedSource
import okio.buffer
import okio.source
import okio.use
import java.net.URI
import java.net.URL
import java.util.*
import java.util.concurrent.ConcurrentHashMap
import kotlin.coroutines.CoroutineContext

@Suppress("KDocMissingDocumentation")
@OptIn(InternalAPI::class, DelicateCoroutinesApi::class)
class NextcloudEngine(override val config: NextcloudConfig) : HttpClientEngineBase("ktor-nextcloud") {

    override val supportedCapabilities: Set<HttpClientEngineCapability<*>> = setOf(HttpTimeoutCapability)

    private val requestsJob: CoroutineContext

    override val coroutineContext: CoroutineContext

    private val context = requireNotNull(config.context)

    private val ssoAccount = requireNotNull(config.ssoAccount)

    init {
        val parent = super.coroutineContext[Job]!!
        requestsJob = SilentSupervisor(parent)
        coroutineContext = super.coroutineContext + requestsJob

        @OptIn(ExperimentalCoroutinesApi::class)
        GlobalScope.launch(super.coroutineContext, start = CoroutineStart.ATOMIC) {
            try {
                requestsJob[Job]!!.join()
            } finally {
                initializedApis.keys.forEach(Companion::resetInitializedApi)
            }
        }
    }

    override suspend fun execute(data: HttpRequestData): HttpResponseData {
        if (!data.url.toString().startsWith(ssoAccount.url)) {
            val delegate = config.delegate ?: error("Please configure a delegate to allow non nextcloud requests.")
            return delegate.engine.execute(data)
        }
        val callContext = callContext()
        val engineRequest = data.convertToNextcloudRequest(callContext, ssoAccount, context)
        var requestEngine = initializedApis[ssoAccount.name]
        val didInitialize = if (requestEngine == null) {
            requestEngine = NextcloudAPI(
                context,
                ssoAccount,
                GsonBuilder().create(),
                object : NextcloudAPI.ApiConnectedListener {
                    override fun onConnected() {
                        Log.v(TAG, "SSO API successfully initialized")
                    }

                    override fun onError(ex: Exception) {
                        Log.e(TAG, ex.message, ex)
                    }
                })
            initializedApis[ssoAccount.name] = requestEngine
            true
        } else {
            false
        }

        return try {
            when {
                data.isUpgradeRequest() -> TODO("executeWebSocketRequest(requestEngine, engineRequest, callContext)")
                data.isSseRequest() -> TODO("executeServerSendEventsRequest(requestEngine, engineRequest, callContext)")
                else -> executeHttpRequest(requestEngine, engineRequest, callContext, data)
            }
        } catch (e: TokenMismatchException) {
            Log.w(
                TAG,
                "NextcloudEngine failed with TokenMismatchException"
            )
            resetInitializedApi(ssoAccount.name)
            if (didInitialize) {
                Log.i(
                    TAG,
                    "This API instance failed at the very first call, so we won't try to re-initialize the API this time…"
                )
                throw e
            } else {
                Log.i(TAG, "This API instance worked before, so we try to re-initialize it…")
                execute(data)
            }
        }
    }

    override fun close() {
        super.close()
        (requestsJob[Job] as CompletableJob).complete()
    }

    private suspend fun executeHttpRequest(
        engine: NextcloudAPI,
        engineRequest: NextcloudRequest,
        callContext: CoroutineContext,
        requestData: HttpRequestData
    ): HttpResponseData {
        val requestTime = GMTDate()
        val response = engine.execute(engineRequest, requestData)

        val body = response.body
        callContext[Job]!!.invokeOnCompletion { body?.close() }

        val responseContent = body?.source()?.buffer()?.toChannel(callContext, requestData) ?: ByteReadChannel.Empty
        return buildResponseData(response, requestTime, responseContent, callContext)
    }

    private fun buildResponseData(
        response: Response,
        requestTime: GMTDate,
        body: Any,
        callContext: CoroutineContext
    ): HttpResponseData {
        val headers = response.plainHeaders.fromOkHttp()

        return HttpResponseData(
            HttpStatusCode.OK,
            requestTime,
            headers,
            HttpProtocolVersion.HTTP_1_1,
            body,
            callContext
        )
    }

    companion object {
        private val initializedApis: MutableMap<String, NextcloudAPI> = ConcurrentHashMap()

        private fun resetInitializedApi(key: String) {
            initializedApis[key]?.close()
            initializedApis.remove(key)
        }
    }
}

private fun ArrayList<AidlNetworkRequest.PlainHeader>.fromOkHttp(): Headers = object : Headers {
    override val caseInsensitiveName: Boolean = true

    override fun getAll(name: String): List<String>? =
        this@fromOkHttp.filter { it.name == name }.map { it.value }.takeIf { it.isNotEmpty() }

    override fun names(): Set<String> = this@fromOkHttp.map { it.name }.toSet()

    override fun entries(): Set<Map.Entry<String, List<String>>> =
        this@fromOkHttp.toMultimap(name = { name }, value = { value }).entries

    override fun isEmpty(): Boolean = this@fromOkHttp.size == 0
}

fun <T> List<T>.toMultimap(name: T.() -> String, value: T.() -> String): Map<String, List<String>> {
    val result = TreeMap<String, MutableList<String>>(String.CASE_INSENSITIVE_ORDER)
    for (i in 0 until size) {
        val name = get(i).name().lowercase(Locale.US)
        var values: MutableList<String>? = result[name]
        if (values == null) {
            values = ArrayList(2)
            result[name] = values
        }
        values.add(get(i).value())
    }
    return result
}

private fun NextcloudAPI.execute(engineRequest: NextcloudRequest, requestData: HttpRequestData): Response {
    // Todo: check if we can make this cancelable
    return this.performNetworkRequestV2(engineRequest)
}

@OptIn(DelicateCoroutinesApi::class)
private fun BufferedSource.toChannel(context: CoroutineContext, requestData: HttpRequestData): ByteReadChannel =
    GlobalScope.writer(context) {
        use { source ->
            var lastRead = 0
            while (source.isOpen && context.isActive && lastRead >= 0) {
                channel.write { buffer ->
                    lastRead = try {
                        source.read(buffer)
                    } catch (cause: Throwable) {
                        throw mapExceptions(cause, requestData)
                    }
                }
                channel.flush()
            }
        }
    }.channel

private fun mapExceptions(cause: Throwable, request: HttpRequestData): Throwable = when (cause) {
    is java.net.SocketTimeoutException -> SocketTimeoutException(request, cause)
    else -> cause
}

@OptIn(InternalAPI::class)
private fun HttpRequestData.convertToNextcloudRequest(
    callContext: CoroutineContext,
    ssoAccount: SingleSignOnAccount,
    context: Context,
): NextcloudRequest {
    val builder = NextcloudRequest.Builder()

    val absoluteUrl = getAbsoluteUrl(ssoAccount, url.toString(), context.fallbackPreviewSize)

    with(builder) {
        // `toASCIIString` encodes path with emoji
        setUrl(URI(absoluteUrl.path.substring(URL(ssoAccount.url).path.length)).toASCIIString())
        setParameter(getQueryParams(absoluteUrl).also(::println))

        val headerList = buildList {
            mergeHeaders(headers, body) { key, value ->
                if (key == HttpHeaders.ContentLength) return@mergeHeaders
                add(key to value)
            }
        }
        setHeader(headerList.toMultimap(name = { first }, value = { second }))

        if (HttpMethod.permitsRequestBody(method.value)) {
            setBody(body, callContext)
        }

        setMethod(method.value)
        // method(method.value, bodyBytes)
    }

    return builder.build()
}

@OptIn(DelicateCoroutinesApi::class)
internal fun NextcloudRequest.Builder.setBody(
    outgoingContent: OutgoingContent,
    callContext: CoroutineContext
): NextcloudRequest.Builder = when (outgoingContent) {
    is OutgoingContent.ByteArrayContent -> setRequestBody(outgoingContent.bytes().toString(Charsets.UTF_8))
    is OutgoingContent.ReadChannelContent -> {
        TODO()
        // StreamRequestBody(outgoingContent.contentLength) { outgoingContent.readFrom() }
    }

    is OutgoingContent.WriteChannelContent -> {
        TODO()
        /* StreamRequestBody(outgoingContent.contentLength) { GlobalScope.writer(callContext) {
            outgoingContent.writeTo(
                this.channel
            )
        }.channel } */
    }

    is OutgoingContent.NoContent -> setRequestBody("")
    else -> throw UnsupportedContentTypeException(outgoingContent)
}

private const val TAG = "NextcloudEngine"
