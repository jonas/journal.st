package st.journal.api

/*
 * Copyright 2014-2019 JetBrains s.r.o and contributors. Use of this source code is governed by the Apache 2.0 license.
 */

import io.ktor.client.*
import io.ktor.client.engine.*

/**
 * A Android client engine that uses the Nextcloud SSO API.
 *
 * To create the client with this engine, pass it to the `HttpClient` constructor:
 * ```kotlin
 * val client = HttpClient(Nextcloud)
 * ```
 * To configure the engine, pass settings exposed by [NextcloudConfig] to the `engine` method:
 * ```kotlin
 * val client = HttpClient(Nextcloud) {
 *     engine {
 *         // this: NextcloudConfig
 *     }
 * }
 * ```
 *
 * You can learn more about client engines from [Engines](https://ktor.io/docs/http-client-engines.html).
 */
object Nextcloud : HttpClientEngineFactory<NextcloudConfig> {
    override fun create(block: NextcloudConfig.() -> Unit): HttpClientEngine =
        NextcloudEngine(NextcloudConfig().apply(block))
}

@Suppress("KDocMissingDocumentation")
class NextcloudEngineContainer : HttpClientEngineContainer {
    override val factory: HttpClientEngineFactory<*> = Nextcloud

    override fun toString(): String = "Nextcloud"
}
