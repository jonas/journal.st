import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import st.journal.appContext

actual fun getDataStore(): DataStore<Preferences> =
    createDataStore(
        producePath = { appContext.filesDir.resolve(dataStoreFileName).absolutePath }
    )