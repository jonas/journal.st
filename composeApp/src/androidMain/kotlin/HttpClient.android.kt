import com.nextcloud.android.sso.helper.SingleAccountHelper
import io.ktor.client.*
import st.journal.api.Nextcloud
import st.journal.appContext

actual fun getCustomHttpClient(config: HttpClientConfig<*>.() -> Unit): HttpClient? = HttpClient(Nextcloud) {
    config(this)
    engine {
        ssoAccount = SingleAccountHelper.getCurrentSingleSignOnAccount(appContext)
        context = appContext
        delegate = HttpClient {
            config(this)
        }
    }
}