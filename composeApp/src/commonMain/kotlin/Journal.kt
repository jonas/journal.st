import androidx.compose.animation.core.animateIntAsState
import androidx.compose.foundation.*
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.interaction.collectIsHoveredAsState
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.grid.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.text.selection.SelectionContainer
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowForward
import androidx.compose.material.icons.filled.AutoFixHigh
import androidx.compose.material.icons.filled.Clear
import androidx.compose.material.icons.filled.Close
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.pointer.PointerIcon
import androidx.compose.ui.input.pointer.pointerHoverIcon
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.platform.LocalUriHandler
import androidx.compose.ui.semantics.Role
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import coil3.annotation.InternalCoilApi
import coil3.compose.AsyncImage
import coil3.compose.LocalPlatformContext
import coil3.network.httpHeaders
import coil3.request.ImageRequest
import coil3.util.MimeTypeMap
import io.ktor.http.*
import io.ktor.util.*
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import kotlinx.datetime.DayOfWeek
import nextcloud.activities.Data
import st.journal.ListDetailLayout
import st.journal.data.Day
import st.journal.data.Week
import st.journal.data.getPastWeeks

@Composable
inline fun LazyGridState.scrollEndCallback(buffer: Int = 0, extraKey: Any? = null, crossinline callback: () -> Unit) {
    LaunchedEffect(this, buffer, extraKey) {
        snapshotFlow { layoutInfo }
            .filter { it.totalItemsCount > 0 }
            .map { it.totalItemsCount <= (it.visibleItemsInfo.lastOrNull()?.index ?: -1) + 1 + buffer }
            .filter { it }
            .onEach { callback() }
            .collect {}
    }
}

enum class Source {
    Manual,
    Auto;
}

sealed class ShowDetailState(val source: Source) {
    class Closed(source: Source): ShowDetailState(source)
    class Open(source: Source): ShowDetailState(source)
}

@Composable
fun Journal(
    nextcloudLogin: NextcloudLogin,
    nextcloudActivities: NextcloudActivities,
    nextcloudBookmarks: NextcloudBookmarks
) {
    val activitiesUiState by nextcloudActivities.state.collectAsState()
    val activities = activitiesUiState.activities
    val iterator = remember(activities) { getPastWeeks(activities).iterator() }
    val items = remember(iterator) {
        buildList {
            repeat(7) {
                add(iterator.next())
            }
        }.toMutableStateList()
    }
    var selectedItem by remember { mutableStateOf(items.first().days.first()) }
    var showDetail by remember { mutableStateOf<ShowDetailState>(ShowDetailState.Closed(Source.Auto)) }
    val gridState = rememberLazyGridState()
    gridState.scrollEndCallback(buffer = 25, extraKey = iterator) {
        items += iterator.next()
    }
    ListDetailLayout(
        list = { Grid(gridState, nextcloudLogin, nextcloudActivities, nextcloudBookmarks, items, onSelectItem = { item ->
            selectedItem = item
            showDetail = ShowDetailState.Open(Source.Manual)
        }) },
        detail = {
            if (showDetail is ShowDetailState.Open) {
                Box(
                    modifier = Modifier
                        .padding(vertical = 16.dp)
                        .padding(end = 16.dp)
                        .clip(MaterialTheme.shapes.extraLarge)
                        .fillMaxSize()
                        .background(MaterialTheme.colorScheme.surface)
                        .padding(all = 16.dp)
                ) {
                    Column {
                        Entry(day = selectedItem)
                        Images(activities = selectedItem.activities, nextcloudLogin)
                    }
                    IconButton(
                        onClick = { showDetail = ShowDetailState.Closed(Source.Manual) },
                        modifier = Modifier.pointerHoverIcon(PointerIcon.Default).align(Alignment.TopEnd)
                    ) {
                        Icon(
                            Icons.Default.Close,
                            contentDescription = "Close detail",
                        )
                    }
                }
            }
        },
        modifier = Modifier.background(MaterialTheme.colorScheme.surfaceVariant),
        showDetailBeside = {
            (it.maxWidth > 700.dp.roundToPx()).also { showDetailBeside ->
                if (showDetailBeside && showDetail is ShowDetailState.Closed && showDetail.source != Source.Manual) {
                    showDetail = ShowDetailState.Open(Source.Auto) // always show detail on large enough screens
                }
            }
        }
    )
}

@OptIn(InternalCoilApi::class, ExperimentalLayoutApi::class)
@Composable
fun Images(activities: List<Data>, nextcloudLogin: NextcloudLogin) {
    val loginState = nextcloudLogin.state.collectAsState().value
    if (loginState !is NextcloudLoginState.LoggedIn) return
    FlowRow(modifier = Modifier.verticalScroll(rememberScrollState())) {
        activities.filter {
            it.type == "file_created"
        }.flatMap { it.objects?.entries ?: emptySet() }.filter {
            MimeTypeMap.getMimeTypeFromUrl(
                it.value
            ).orEmpty().startsWith("image")
        }.forEach { image ->
            AsyncImage(
                model = ImageRequest.Builder(LocalPlatformContext.current)
                    .data("${loginState.serverUrl}/index.php/core/preview?fileId=${image.key}&x=100&y=200&a=true")
                    .httpHeaders(Headers.build {
                        set(
                            HttpHeaders.Authorization,
                            "Basic ${"${loginState.username}:${loginState.password}".encodeBase64()}"
                        )
                    // set("OCS-APIREQUEST", "true")
                    })
                    // .placeholderMemoryCacheKey(screen.placeholder)
                    // .apply { extras.setAll(screen.image.extras) }
                    .build(),
                contentDescription = null,
                modifier = Modifier.height(100.dp),
                )
        }
    }
}

@Composable
fun Grid(
    gridState: LazyGridState,
    nextcloudLogin: NextcloudLogin,
    nextcloudActivities: NextcloudActivities,
    nextcloudBookmarks: NextcloudBookmarks,
    items: SnapshotStateList<Week>,
    onSelectItem: (Day) -> Unit
) {
    LazyVerticalGrid(
        columns = GridCells.Adaptive(250.dp),
        modifier = Modifier.fillMaxSize(),
        state = gridState,
        verticalArrangement = Arrangement.spacedBy(16.dp),
        horizontalArrangement = Arrangement.spacedBy(16.dp),
        contentPadding = PaddingValues(16.dp),
    ) {
        item(span = { GridItemSpan(maxLineSpan) }) {
            NextcloudLogin(nextcloudLogin)
            Column(verticalArrangement = Arrangement.spacedBy(16.dp)) {
                FetchNextcloudActivities(nextcloudLogin, nextcloudActivities)
                FetchNextcloudBookmarks(nextcloudLogin, nextcloudBookmarks)
            }
        }

        items.forEach { week ->
            item(span = { GridItemSpan(maxLineSpan) }) {
                Spacer(modifier = Modifier.height(24.dp))
            }

            item(span = { GridItemSpan(if (maxLineSpan >= 3) 2 else 1) }) {
                Row {
                    Text(
                        "Week ${week.number}",
                        modifier = Modifier.alignByBaseline(),
                        style = MaterialTheme.typography.headlineLarge
                    )
                    Text(
                        " — see what happened.",
                        modifier = Modifier.alignByBaseline(),
                        style = MaterialTheme.typography.bodyLarge
                    )
                }
            }
            items(week.days) { day ->
                Entry(day = day, onSelect = { onSelectItem(day) })
            }
        }
    }
}

@Composable
fun NextcloudLogin(nextcloudLogin: NextcloudLogin) {
    val coroutineScope = rememberCoroutineScope()
    val loginState by nextcloudLogin.state.collectAsState()
    var serverUrl by remember { mutableStateOf("") }
    val uriHandler = LocalUriHandler.current
    if (loginState is NextcloudLoginState.LoggedIn) return
    var loginJob: Job? by remember { mutableStateOf(null) }
    fun login() {
        loginJob = coroutineScope.launch {
            nextcloudLogin.login(serverUrl, uriHandler)
        }
    }
    Spacer(modifier = Modifier.height(24.dp))
    Row(horizontalArrangement = Arrangement.spacedBy(16.dp), verticalAlignment = Alignment.CenterVertically) {
        if (loginState == NextcloudLoginState.ServerUrlNeeded || loginState == NextcloudLoginState.LoginInProgress) {
            OutlinedTextField(
                serverUrl,
                onValueChange = { serverUrl = it },
                enabled = loginState == NextcloudLoginState.ServerUrlNeeded,
                label = { Text("Server address https://...") },
                trailingIcon = {
                    if (loginState == NextcloudLoginState.ServerUrlNeeded) {
                        IconButton(onClick = ::login, modifier = Modifier.pointerHoverIcon(PointerIcon.Default)) {
                            Icon(
                                Icons.Default.ArrowForward,
                                contentDescription = "Login with Nextcloud",
                            )
                        }
                    } else {
                        val interactionSource = remember { MutableInteractionSource() }
                        val isHovered by interactionSource.collectIsHoveredAsState()
                        IconButton(
                            onClick = {
                                loginJob?.cancel()
                                loginJob = null
                            },
                            modifier = Modifier.hoverable(interactionSource).pointerHoverIcon(PointerIcon.Default)
                        ) {
                            if (isHovered) {
                                Icon(
                                    Icons.Default.Clear,
                                    contentDescription = "Cancel login",
                                )
                            } else {
                                CircularProgressIndicator(strokeWidth = 2.dp, modifier = Modifier.size(20.dp))
                            }
                        }
                    }
                },
                singleLine = true,
                keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Uri, imeAction = ImeAction.Go),
                keyboardActions = KeyboardActions(
                    onDone = { login() },
                    onGo = { login() }
                )
            )
        } else {
            Button(
                onClick = {
                    coroutineScope.launch {
                        nextcloudLogin.login(serverUrl, uriHandler)
                    }
                },
                modifier = Modifier.widthIn(max = 320.dp)
            ) {
                Text("Login")
            }
        }
    }
}

@OptIn(InternalCoilApi::class)
@Composable
fun FetchNextcloudActivities(nextcloudLogin: NextcloudLogin, nextcloudActivities: NextcloudActivities) {
    val loginState = nextcloudLogin.state.collectAsState().value
    if (loginState !is NextcloudLoginState.LoggedIn) return
    val state by nextcloudActivities.state.collectAsState()
    val coroutineScope = rememberCoroutineScope()
    var showDebugInfo by remember { mutableStateOf(false) }
    Column {
        Row(horizontalArrangement = Arrangement.spacedBy(16.dp)) {
            Button(
                onClick = {
                    coroutineScope.launch {
                        nextcloudActivities.fetchActivities()
                    }
                },
                enabled = state.loadingState != NextcloudActivitiesLoadingState.Loading
            ) {
                if (state.loadingState == NextcloudActivitiesLoadingState.Loading) {
                    CircularProgressIndicator(strokeWidth = 2.dp, modifier = Modifier.size(20.dp))
                } else {
                    Text("Get Activities")
                }
            }
            if (state.activities != null) {
                Button(
                    onClick = { showDebugInfo = !showDebugInfo },
                ) {
                    Text((if (showDebugInfo) "Hide" else "Show") + " Debug Info")
                }
            }
        }
        if (!showDebugInfo) return@Column
        state.activities?.ocs?.data?.filter {
            it.type == "file_created"
        }?.flatMap { it.objects?.entries ?: emptySet() }?.filter {
            MimeTypeMap.getMimeTypeFromUrl(
                it.value
            ).orEmpty().startsWith("image")
        }?.forEach { image ->
            Row {
                AsyncImage(
                    model = ImageRequest.Builder(LocalPlatformContext.current)
                        .data("${loginState.serverUrl}/index.php/core/preview?fileId=${image.key}&x=100&y=200&a=true")
                        .httpHeaders(Headers.build {
                            set(
                                HttpHeaders.Authorization,
                                "Basic ${"${loginState.username}:${loginState.password}".encodeBase64()}"
                            )
                            // set("OCS-APIREQUEST", "true")
                        })
                        // .placeholderMemoryCacheKey(screen.placeholder)
                        // .apply { extras.setAll(screen.image.extras) }
                        .build(),
                    contentDescription = null,
                    modifier = Modifier.size(100.dp),
                )
                AsyncImage(
                    model = ImageRequest.Builder(LocalPlatformContext.current)
                        .data("${loginState.serverUrl}/remote.php/webdav${image.value}")
                        .httpHeaders(Headers.build {
                            set(
                                HttpHeaders.Authorization,
                                "Basic ${"${loginState.username}:${loginState.password}".encodeBase64()}"
                            )
                            // set("OCS-APIREQUEST", "true")
                        })
                        // .placeholderMemoryCacheKey(screen.placeholder)
                        // .apply { extras.setAll(screen.image.extras) }
                        .build(),
                    contentDescription = null,
                    modifier = Modifier.size(100.dp),
                )
            }
        }
        state.activities?.ocs?.let {
            SelectionContainer {
                Text(it.toString())
            }
        }
    }
}

@Composable
fun FetchNextcloudBookmarks(nextcloudLogin: NextcloudLogin, nextcloudBookmarks: NextcloudBookmarks) {
    val loginState = nextcloudLogin.state.collectAsState().value
    if (loginState !is NextcloudLoginState.LoggedIn) return
    val state by nextcloudBookmarks.state.collectAsState()
    val coroutineScope = rememberCoroutineScope()
    var showDebugInfo by remember { mutableStateOf(false) }
    Column {
        Row(horizontalArrangement = Arrangement.spacedBy(16.dp)) {
            Button(
                onClick = {
                    coroutineScope.launch {
                        nextcloudBookmarks.fetchBookmarks()
                    }
                },
                enabled = state.loadingState != NextcloudBookmarksLoadingState.Loading
            ) {
                if (state.loadingState == NextcloudBookmarksLoadingState.Loading) {
                    CircularProgressIndicator(strokeWidth = 2.dp, modifier = Modifier.size(20.dp))
                } else {
                    Text("Get Bookmarks")
                }
            }
            if (state.bookmarks != null) {
                Button(
                    onClick = { showDebugInfo = !showDebugInfo },
                ) {
                    Text((if (showDebugInfo) "Hide" else "Show") + " Debug Info")
                }
            }
        }
        if (!showDebugInfo) return@Column
        state.bookmarks?.let {
            SelectionContainer {
                Text(it.toString())
            }
        }
    }
}

val ids = '⡀'..'⣿' // 191

val DayOfWeek.displayName get() = name[0] + name.lowercase().substring(1)

@Composable
fun Entry(day: Day, onSelect: () -> Unit = {}) {
    var showFullCode by remember { mutableStateOf(false) }
    val code = day.code
    val codeLength: Int by animateIntAsState(if (showFullCode) code.length else 8)
    Row(
        modifier = Modifier
            .clip(RoundedCornerShape(4.dp))
            .clickable(role = Role.Button) {
                showFullCode = !showFullCode
                onSelect()
            }
            .padding(8.dp),
        horizontalArrangement = Arrangement.spacedBy(16.dp)
    ) {
        Code(code.substring(0, codeLength))
        Column {
            Text(day.date.dayOfWeek.displayName, style = MaterialTheme.typography.headlineSmall)
            Text(day.date.toString(), style = MaterialTheme.typography.bodyLarge)
            Row {
                if (day.activities.isNotEmpty()) {
                    val amount = day.activities.fold(0) { acc, data -> acc + (data.objects?.size ?: 0) }
                    Text("$amount", style = MaterialTheme.typography.bodyMedium)
                    Icon(
                        Icons.Default.AutoFixHigh,
                        contentDescription = "Attachable activities",
                        modifier = Modifier.height(with(LocalDensity.current) { MaterialTheme.typography.bodyMedium.fontSize.toDp() })
                    )
                    Spacer(modifier = Modifier.width(8.dp))
                }
                Text("Lorem Ipsum", style = MaterialTheme.typography.bodyMedium)
            }
        }
    }
}

@Composable
fun Code(code: String) {
    val byColumn = 4
    val byRow = code.length / byColumn
    Canvas(
        modifier = Modifier.height(48.dp).aspectRatio(byRow.toFloat() / byColumn)
    ) {
        val spacing = size.height / byColumn / 4
        val radius = size.height / byColumn / 2 - spacing

        repeat(byRow - 1) {
            val x = (radius * 2 + spacing * 2) * (it + 1)
            drawLine(Color.DarkGray, Offset(x, 0f), Offset(x, size.height))
        }
        repeat(byColumn - 1) {
            val y = (radius * 2 + spacing * 2) * (it + 1)
            drawLine(Color.DarkGray, Offset(0f, y), Offset(size.width, y))
        }

        for ((c, column) in code.chunked(byColumn).withIndex()) {
            for ((r, bit) in column.withIndex()) {
                if (bit == '1') {
                    drawCircle(
                        Color.Black,
                        radius,
                        center = Offset(
                            x = c * 2 * radius + radius + 2 * spacing * c + spacing,
                            y = r * 2 * radius + radius + 2 * spacing * r + spacing
                        )
                    )
                }
            }
        }
    }
}
