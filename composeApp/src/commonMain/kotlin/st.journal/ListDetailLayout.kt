package st.journal

import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.Layout
import androidx.compose.ui.layout.MeasureScope
import androidx.compose.ui.unit.Constraints
import androidx.compose.ui.unit.dp

@Composable
fun ListDetailLayout(
    list: @Composable () -> Unit,
    detail: @Composable () -> Unit,
    modifier: Modifier = Modifier,
    showDetailBeside: MeasureScope.(Constraints) -> Boolean = { it.maxWidth > 700.dp.roundToPx() }
) {
    Layout(contents = listOf(list, detail), modifier = modifier) { (listMeasurables, detailMeasurables), constraints ->
        require(listMeasurables.size == 1) { "Only pass a single list Composable." }
        require(detailMeasurables.size <= 1) { "Only pass a single detail Composable." }
        if (showDetailBeside(constraints)) {
            val listWidth = (constraints.maxWidth * 0.60f).toInt()
            val listPlacable = listMeasurables.first().measure(constraints.copy(maxWidth = listWidth, minWidth = listWidth))
            val detailPlacable = detailMeasurables.firstOrNull()?.measure(constraints.copy(maxWidth = constraints.maxWidth - listWidth))
            layout(constraints.maxWidth, constraints.maxHeight) {
                listPlacable.placeRelative(x = 0, y = 0)
                detailPlacable?.placeRelative(x = listWidth, y = 0)
            }
        } else {
            val listPlacable = listMeasurables.first().measure(constraints)
            val detailPlacable = detailMeasurables.firstOrNull()?.measure(constraints)
            layout(constraints.maxWidth, constraints.maxHeight) {
                listPlacable.placeRelative(x = 0, y = 0)
                detailPlacable?.placeRelative(x = 0, y = 0)
            }
        }
    }
}