package st.journal.data

import kotlinx.datetime.Clock
import kotlinx.datetime.LocalDate
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toLocalDateTime
import kotlin.time.Duration.Companion.days

val LocalDate.weekNumber: Int
    get() {
        val firstDayOfYear = LocalDate(year, 1, 1)
        val daysFromFirstDay = dayOfYear - firstDayOfYear.dayOfYear
        val firstDayOfYearDayOfWeek = firstDayOfYear.dayOfWeek.ordinal
        val adjustment = when {
            firstDayOfYearDayOfWeek <= 4 -> firstDayOfYearDayOfWeek - 1
            else -> 8 - firstDayOfYearDayOfWeek
        }
        return (daysFromFirstDay + adjustment) / 7 + 1
    }

val pastWeeks = sequence {
    val pastDays = generateSequence(Clock.System.now()) {
        it - 1.days
    }.map { it.toLocalDateTime(TimeZone.currentSystemDefault()).date }
    val dayOfWeek = pastDays.first().dayOfWeek
    yield(pastDays.take(dayOfWeek.ordinal + 1).toList())
    yieldAll(pastDays.drop(dayOfWeek.ordinal + 1).chunked(7))
}