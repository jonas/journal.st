package st.journal.data

import kotlinx.datetime.LocalDate
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toLocalDateTime
import nextcloud.activities.Activities
import nextcloud.activities.Data
import kotlin.random.Random

val random = Random.Default

fun getRandomCode() = random.nextBytes(3).map { it.toInt() + Byte.MAX_VALUE + 1 }
    .joinToString("") { it.toString(2).padStart(8, '0') }

class Day(val code: String, val date: LocalDate, val activities: List<Data>)
class Week(val number: Int, val days: List<Day>)

fun getPastWeeks(activities: Activities?) = pastWeeks.map { days ->
    Week(number = days.first().weekNumber, days = days.map { date ->
        Day(code = getRandomCode(), date = date, activities = activities?.ocs?.data?.filter {
            it.datetime.toLocalDateTime(TimeZone.currentSystemDefault()).date == date
        }.orEmpty())
    })
}