import io.ktor.client.*
import io.ktor.client.plugins.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.plugins.logging.*
import io.ktor.http.*
import io.ktor.serialization.kotlinx.json.*
import kotlinx.serialization.json.Json

expect fun getCustomHttpClient(config: HttpClientConfig<*>.() -> Unit): HttpClient?

val httpClient by lazy {
    val config: HttpClientConfig<*>.() -> Unit = {
        install(ContentNegotiation) {
            json(Json {
                ignoreUnknownKeys = true
            })
        }
        install(UserAgent) {
            agent = "Journalist ${getPlatform().name}"
        }
        install(Logging) {
            logger = Logger.SIMPLE
            sanitizeHeader { header -> header == HttpHeaders.Authorization }
        }
    }
    getCustomHttpClient(config) ?: HttpClient(config)
}