import NextcloudLoginState.*
import androidx.compose.ui.platform.UriHandler
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import io.ktor.client.call.*
import io.ktor.client.request.*
import io.ktor.client.request.forms.*
import io.ktor.client.statement.*
import io.ktor.http.*
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import kotlinx.serialization.Serializable
import kotlin.time.Duration.Companion.minutes
import kotlin.time.Duration.Companion.seconds

sealed class NextcloudLoginState {
    data object ServerUrlNeeded : NextcloudLoginState()
    data object ReadyForLogin : NextcloudLoginState()
    data object LoginInProgress : NextcloudLoginState()
    data class LoggedIn(val serverUrl: String, val username: String? = null, val password: String? = null) : NextcloudLoginState()
}

interface NextcloudLogin {
    val state: StateFlow<NextcloudLoginState>
    suspend fun login(serverUrl: String? = null, uriHandler: UriHandler? = null)
}

fun HttpMessageBuilder.basicAuth(loggedInState: NextcloudLoginState.LoggedIn) {
    if (loggedInState.username == null || loggedInState.password == null) return
    basicAuth(loggedInState.username, loggedInState.password)
    header("OCS-APIREQUEST", true)
}

class DefaultNextcloudLogin : NextcloudLogin {

    private val _state = MutableStateFlow<NextcloudLoginState>(ServerUrlNeeded)
    override val state = _state.asStateFlow()

    init {
        // Todo: replace with proper viewmodel implementation
        GlobalScope.launch {
            dataStore.loggedIn.collect { loggedIn -> _state.update { loggedIn } }
        }
    }

    override suspend fun login(serverUrl: String?, uriHandler: UriHandler?) {
        try {
            if (serverUrl.isNullOrBlank() || uriHandler == null) return
            _state.update { LoginInProgress }
            val loginV2Response = try {
                httpClient.post(serverUrl) {
                    url {
                        appendPathSegments("index.php", "login", "v2")
                    }
                }
            } catch (e: Exception) {
                _state.update { ServerUrlNeeded }
                return
            }
            if (!loginV2Response.status.isSuccess()) {
                _state.update { ServerUrlNeeded }
                return
            }
            val loginV2: LoginV2 = loginV2Response.body()
            uriHandler.openUri(loginV2.login)
            try {
                loginV2.poll()
            } catch (e: TimeoutCancellationException) {
                _state.update { ServerUrlNeeded }
            } catch (e: IllegalStateException) {
                println(e.message)
                _state.update { ServerUrlNeeded }
            }
        } catch (e: CancellationException) {
            _state.update { ServerUrlNeeded }
        }
    }

    private suspend operator fun LoginV2.Poll.invoke() = withTimeout(20.minutes) {
        while (true) {
            val res = httpClient.submitForm(
                url = endpoint,
                formParameters = parameters {
                    append("token", token)
                },
                encodeInQuery = true
            ) {
                method = HttpMethod.Post
            }
            when (res.status.value) {
                200 -> {
                    val credentials: LoginV2PollResponse = res.body()
                    dataStore.save(credentials)
                    return@withTimeout
                }
                404 -> { /* Expected while user loggs in. */ }
                else -> error("Unexpected result from polling endpoint: ${res.status.value} - ${res.bodyAsText()}")
            }
            delay(5.seconds)
        }
    }
}

val serverUrlKey = stringPreferencesKey("server_url")
val usernameKey = stringPreferencesKey("username")
val passwordKey = stringPreferencesKey("password")

private suspend fun DataStore<Preferences>.save(credentials: LoginV2PollResponse) = edit { preferences ->
    preferences[serverUrlKey] = credentials.server
    preferences[usernameKey] = credentials.loginName
    preferences[passwordKey] = credentials.appPassword
}

private val DataStore<Preferences>.loggedIn: Flow<LoggedIn> get() = data.map { preferences ->
    val serverUrl = preferences[serverUrlKey]
    val username = preferences[usernameKey]
    val password = preferences[passwordKey]
    serverUrl?.let { LoggedIn(serverUrl = serverUrl, username = username, password = password) }
}.filterNotNull()

@Serializable
data class LoginV2(val poll: Poll, val login: String) {
    @Serializable
    data class Poll(val token: String, val endpoint: String)
}

@Serializable
data class LoginV2PollResponse(val server: String, val loginName: String, val appPassword: String)