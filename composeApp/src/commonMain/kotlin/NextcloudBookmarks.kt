import NextcloudBookmarksLoadingState.*
import io.ktor.client.call.*
import io.ktor.client.request.*
import io.ktor.http.*
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import nextcloud.bookmarks.bookmarks.Bookmark
import nextcloud.bookmarks.bookmarks.CreateBookmark
import nextcloud.bookmarks.bookmarks.QueryBookmarks
import nextcloud.bookmarks.folders.CreateFolder
import nextcloud.bookmarks.folders.EditFolder
import nextcloud.bookmarks.folders.FullFolderHierarchy

enum class NextcloudBookmarksLoadingState {
    Idle, Loading, Loaded
}

data class NextcloudBookmarksUiState(
    val loadingState: NextcloudBookmarksLoadingState,
    val bookmarks: List<Bookmark>? = null
)

class NextcloudBookmarks(private val nextcloudLogin: NextcloudLogin) {
    private val _state =
        MutableStateFlow(NextcloudBookmarksUiState(loadingState = Idle))
    val state = _state.asStateFlow()

    suspend fun fetchBookmarks() {
        // return createFolder(CreateFolder.Request(title = "2024-01-21", parentFolder = 765))
        // return editFolder(id = 765, EditFolder.Request(title = "Journalist"))
        // return fetchFullFolderHierarchy(root = 765, layers = 1)
        // return createBookmark(CreateBookmark.Request(url = , folders = listOf(766)))
        try {
            val loggedInState =
                nextcloudLogin.state.value as? NextcloudLoginState.LoggedIn ?: error("Please log in first.")
            _state.update { it.copy(loadingState = Loading) }
            val bookmarksResponse = try {
                httpClient.get(loggedInState.serverUrl) {
                    url {
                        appendPathSegments("/index.php/apps/bookmarks/public/rest/v2/bookmark") // ?page=-1
                    }
                    basicAuth(loggedInState)
                }
            } catch (e: Exception) {
                _state.update { it.copy(loadingState = Idle) }
                return
            }
            val body: QueryBookmarks.Response = bookmarksResponse.body()
            _state.update { it.copy(loadingState = Loaded, bookmarks = body.data) }
        } catch (e: CancellationException) {
            _state.update { it.copy(loadingState = Idle) }
        }
    }
    
    suspend fun createBookmark(bookmark: CreateBookmark.Request) {
        val loggedInState =
            nextcloudLogin.state.value as? NextcloudLoginState.LoggedIn ?: error("Please log in first.")
        val createBookmarkResponse = try {
            httpClient.post(loggedInState.serverUrl) {
                url {
                    appendPathSegments("/index.php/apps/bookmarks/public/rest/v2/bookmark")
                }
                contentType(ContentType.Application.Json)
                setBody(bookmark)
                basicAuth(loggedInState)
            }
        } catch (e: Exception) {
            println(e.message)
            return
        }
        val body: CreateBookmark.Response = createBookmarkResponse.body()
        println(body.toString())
    }
    
    suspend fun createFolder(folder: CreateFolder.Request) {
            val loggedInState =
                nextcloudLogin.state.value as? NextcloudLoginState.LoggedIn ?: error("Please log in first.")
            val createFolderResponse = try {
                httpClient.post(loggedInState.serverUrl) {
                    url {
                        appendPathSegments("/index.php/apps/bookmarks/public/rest/v2/folder")
                    }
                    contentType(ContentType.Application.Json)
                    setBody(folder)
                    basicAuth(loggedInState)
                }
            } catch (e: Exception) {
                println(e.message)
                return
            }
            val body: CreateFolder.Response = createFolderResponse.body()
            println(body.toString())
    }
    
    suspend fun editFolder(id: Int, folder: EditFolder.Request) {
        val loggedInState =
            nextcloudLogin.state.value as? NextcloudLoginState.LoggedIn ?: error("Please log in first.")
        val editFolderResponse = try {
            httpClient.put(loggedInState.serverUrl) {
                url {
                    appendPathSegments("/index.php/apps/bookmarks/public/rest/v2/folder", "$id")
                }
                contentType(ContentType.Application.Json)
                setBody(folder)
                basicAuth(loggedInState)
            }
        } catch (e: Exception) {
            println(e.message)
            return
        }
        val body: EditFolder.Response = editFolderResponse.body()
        println(body.toString())
    }
    
    suspend fun fetchFullFolderHierarchy(root: Int? = null, layers: Int? = null) {
        val loggedInState =
            nextcloudLogin.state.value as? NextcloudLoginState.LoggedIn ?: error("Please log in first.")
        val fullFolderHierarchyResponse = try {
            httpClient.get(loggedInState.serverUrl) {
                url {
                    appendPathSegments("/index.php/apps/bookmarks/public/rest/v2/folder")
                    root?.let { parameters.append("root", "$root") }
                    layers?.let { parameters.append("layers", "$root") }
                }
                basicAuth(loggedInState)
            }
        } catch (e: Exception) {
            println(e.message)
            return
        }
        val body: FullFolderHierarchy.Response = fullFolderHierarchyResponse.body()
        println(body.toString())
    }
}
