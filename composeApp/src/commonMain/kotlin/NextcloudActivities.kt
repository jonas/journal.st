
import NextcloudActivitiesLoadingState.*
import io.ktor.client.call.*
import io.ktor.client.request.*
import io.ktor.http.*
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import nextcloud.activities.Activities

enum class NextcloudActivitiesLoadingState {
    Idle, Loading, Loaded
}

data class NextcloudActivitiesUiState(val loadingState: NextcloudActivitiesLoadingState, val activities: Activities? = null)

class NextcloudActivities(private val nextcloudLogin: NextcloudLogin) {
    private val _state =
        MutableStateFlow(NextcloudActivitiesUiState(loadingState = Idle))
    val state = _state.asStateFlow()

    suspend fun fetchActivities() {
        try {
            val loggedInState = nextcloudLogin.state.value as? NextcloudLoginState.LoggedIn ?: error("Please log in first.")
            _state.update { it.copy(loadingState = Loading) }
            val activitiesResponse = try {
                httpClient.get(loggedInState.serverUrl) {
                    url {
                        appendPathSegments("/ocs/v2.php/apps/activity/api/v2/activity")
                        parameters.append("limit", "150")
                    }
                    basicAuth(loggedInState)
                }
            } catch (e: Exception) {
                _state.update { it.copy(loadingState = Idle) }
                return
            }
            val body: Activities = activitiesResponse.body()
            _state.update { it.copy(loadingState = Loaded, activities = body) }
        } catch (e: CancellationException) {
            _state.update { it.copy(loadingState = Idle) }
        }
    }
}
