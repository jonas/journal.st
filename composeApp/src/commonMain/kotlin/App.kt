@file:OptIn(ExperimentalCoilApi::class)

import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import coil3.ImageLoader
import coil3.annotation.ExperimentalCoilApi
import coil3.compose.setSingletonImageLoaderFactory
import coil3.network.NetworkFetcher
import coil3.util.DebugLogger

@Composable
fun App(nextcloudLogin: NextcloudLogin = remember { DefaultNextcloudLogin() }) {
    setSingletonImageLoaderFactory { context ->
        ImageLoader.Builder(context)
            .components {
                add(NetworkFetcher.Factory(httpClient = lazy { httpClient }))
            }
            .logger(DebugLogger())
            .build()
    }
    MaterialTheme {
        Journal(
            nextcloudLogin,
            nextcloudActivities = remember(nextcloudLogin) { NextcloudActivities(nextcloudLogin) },
            nextcloudBookmarks = remember(nextcloudLogin) { NextcloudBookmarks(nextcloudLogin) }
        )
    }
}