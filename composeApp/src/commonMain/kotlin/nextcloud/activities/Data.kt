package nextcloud.activities

import kotlinx.datetime.Instant
import kotlinx.serialization.*
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder

@Serializable
data class Data(
    @SerialName("activity_id")
    val activityId: Int,
    val app: String,
    val type: String,
    val user: String? = null,
    val subject: String,
    // @SerialName("subject_rich")
    // val subjectRich: List<Any>? = null,
    val message: String? = null,
    // @SerialName("message_rich")
    // val messageRich: List<String>? = null,
    @SerialName("object_type")
    val objectType: String? = null,
    @SerialName("object_id")
    val objectId: Int? = null,
    @SerialName("object_name")
    val objectName: String? = null,
    @Serializable(with = ObjectOrArraySerializer::class)
    val objects: Map<String, String>? = null,
    val link: String? = null,
    val icon: String? = null,
    val datetime: Instant,
)

/**
 * Hacky way to ignore that sometimes the api returns an empty array instead of an object.
 */
object ObjectOrArraySerializer : KSerializer<Map<String, String>?> {

    val delegateSerializer: KSerializer<Map<String, String>?> = serializer()
    @OptIn(ExperimentalSerializationApi::class)
    override val descriptor: SerialDescriptor = SerialDescriptor("ObjectOrArray", delegateSerializer.descriptor)

    override fun serialize(encoder: Encoder, value: Map<String, String>?) {
        delegateSerializer.serialize(encoder, value)
    }

    override fun deserialize(decoder: Decoder): Map<String, String>? {
        return try {
            delegateSerializer.deserialize(decoder)
        } catch (ignored: Exception) {
            try {
                delegateSerializer.deserialize(decoder)
            } catch (ignored: Exception) {
                println("Ignored returned empty array instead of object.")
            }
            emptyMap()
        }
    }
}
