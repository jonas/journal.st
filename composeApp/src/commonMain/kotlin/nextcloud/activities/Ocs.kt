package nextcloud.activities

import kotlinx.serialization.Serializable

@Serializable
data class Ocs(
    val meta: Meta,
    val data: List<Data>
)