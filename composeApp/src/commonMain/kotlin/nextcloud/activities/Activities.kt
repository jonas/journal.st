package nextcloud.activities

import kotlinx.serialization.Serializable

@Serializable
data class Activities(val ocs: Ocs)