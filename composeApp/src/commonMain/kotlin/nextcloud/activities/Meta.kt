package nextcloud.activities

import kotlinx.serialization.Serializable

@Serializable
data class Meta(
    val status: String,
    val statuscode: Int,
    val message: String
)