package nextcloud.bookmarks.folders

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

object FullFolderHierarchy {
    @Serializable
    data class Response(
        val status: String,
        val data: List<Item>
    ) {
        @Serializable
        data class Item(
            val id: Int,
            val title: String,
            @SerialName("parent_folder")
            val parentFolder: Int? = null,
            val children: List<Item>? = null,
        )
    }
}