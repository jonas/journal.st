package nextcloud.bookmarks.folders

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

object CreateFolder {
    @Serializable
    data class Request(
        val title: String,
        @SerialName("parent_folder")
        val parentFolder: Int? = null,
    )

    @Serializable
    data class Response(
        val status: String,
        val item: Folder
    )
}