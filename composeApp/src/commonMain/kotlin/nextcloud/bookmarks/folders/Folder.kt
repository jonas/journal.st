package nextcloud.bookmarks.folders

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class Folder(
    val id: Int,
    val title: String,
    @SerialName("parent_folder")
    val parentFolder: Int? = null,
)