package nextcloud.bookmarks.bookmarks

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class Bookmark(
    val id: Int,
    val url: String,
    val target: String,
    val title: String,
    val description: String,
    val added: Int,
    val userId: String,
    val tags: List<String>,
    val folders: List<Int>,
    @SerialName("clickcount")
    val clickCount: Int,
    val available: Boolean,
    val htmlContent: String? = null,
    val textContent: String? = null,
    val archivedFile: Int? = null
)