package nextcloud.bookmarks.bookmarks

import kotlinx.serialization.Serializable

object CreateBookmark {
    @Serializable
    data class Request(
        val url: String,
        val tags: List<Int>? = null,
        val title: String? = null,
        val description: String? = null,
        val folders: List<Int>? = null,
    )
    
    @Serializable
    data class Response(
        val status: String,
        val item: Bookmark
    )
}