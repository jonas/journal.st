package nextcloud.bookmarks.bookmarks

import kotlinx.serialization.Serializable

object QueryBookmarks {
    @Serializable
    data class Response(
        val status: String,
        val data: List<Bookmark>
    )
}