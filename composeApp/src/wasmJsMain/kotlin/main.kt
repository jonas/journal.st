import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.platform.UriHandler
import androidx.compose.ui.window.CanvasBasedWindow
import kotlinx.coroutines.flow.MutableStateFlow

@OptIn(ExperimentalComposeUiApi::class)
fun main() {
    CanvasBasedWindow(canvasElementId = "ComposeTarget") { App(CustomNextcloudLogin) }
}

object CustomNextcloudLogin: NextcloudLogin {
    override val state = MutableStateFlow<NextcloudLoginState>(NextcloudLoginState.LoggedIn(""))

    override suspend fun login(serverUrl: String?, uriHandler: UriHandler?) = TODO()

}
