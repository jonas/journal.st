import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences

actual fun getDataStore(): DataStore<Preferences> = error("DataStore can't be used in wasm.")