package androidx.datastore.preferences.core

import androidx.datastore.core.DataStore
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.flow.Flow
import okio.Path

object PreferenceDataStoreFactory {
    public fun createWithPath(
        corruptionHandler: Any? = null,
        migrations: List<Any> = listOf(),
        scope: CoroutineScope = GlobalScope,
        produceFile: () -> Path,
        ): DataStore<Preferences> = object : DataStore<Preferences> {
        override val data: Flow<Preferences>
            get() = TODO("Not yet implemented")

        override suspend fun updateData(transform: suspend (t: Preferences) -> Preferences): Preferences {
            TODO("Not yet implemented")
        }

    }
}