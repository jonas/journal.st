package androidx.datastore.preferences.core

import androidx.datastore.core.DataStore

public abstract class Preferences internal constructor() {
    public class Key<T>
    internal constructor(public val name: String)

    public abstract operator fun <T> get(key: Key<T>): T?
}

public class MutablePreferences internal constructor() : Preferences() {
    override operator fun <T> get(key: Key<T>): T? {
        TODO()
    }

    public operator fun <T> set(key: Key<T>, value: T) {
        TODO()
    }
}

public fun stringPreferencesKey(name: String): Preferences.Key<String> = TODO()

public suspend fun DataStore<Preferences>.edit(
    transform: suspend (MutablePreferences) -> Unit
): Preferences {
    TODO()
}